# Home automation feed

## Description

This is an OpenWrt package feed containing community maintained Smart Home automation packages.

## Usage

To use these packages, add the following line to the feeds.conf
in the OpenWrt buildroot:

```
src-git smarthome https://bitbucket.org/kayo/openwrt-smarthome.git
```

This feed should be included and enabled by default in the OpenWrt buildroot. To install all its package definitions, run:

```
./scripts/feeds update smarthome
./scripts/feeds install -a -p smarthome
```

From now the smarthome packages should now appear in menuconfig.
