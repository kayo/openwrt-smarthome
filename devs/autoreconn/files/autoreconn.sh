#!/bin/sh

ping -W5 -c1 $2 || {
    ubus call network.interface.$1 down
    ubus call network.interface.$1 up
}
