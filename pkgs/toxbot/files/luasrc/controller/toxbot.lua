-- LuCI controller

module("luci.controller.toxbot", package.seeall)

function index()
  if not nixio.fs.access("/etc/config/toxbot") then
    return
  end
  
  local page = entry({"admin", "services", "toxbot"}, cbi("toxbot"), _("ToxBot - Extendable Tox Bot"), 60)
  page.dependent = true
end
