-- LuCI model

local name = "toxbot"

local x = luci.model.uci.cursor()

local m = Map(name, translate("ToxBot - Extendable Tox Bot"),
              translatef("Configure your bots here."))

local s = m:section(TypedSection, name, translate("Settings"))
s.addremove = true
s.anonymous = true

s:option(Flag, "enabled", translate("Enabled"))

s:option(Value, "profile", translate("Profile"), translate("The profile for this bot"))
s:option(Value, "self_name", translate("Bot name"), translate("Default nickname of bot"))

s:option(Flag, "use_udp", translate("Use UDP")).default = 1
s:option(Flag, "use_ipv6", translate("Use IPv6")).default = 1

local l = s:option(ListValue, "language", translate("Language"), translate("Default interface language"))
l:value("", "English")
l:value("ru", "Русский")

--- plugin checkboxes
function list_plugins()
  local command = "toxbot -i"
  function use_lang(s)
    if s and s.language and s.language ~= "" then
      command = command .. " -l " .. s.language
      return true
    end
    return false
  end
  if not use_lang(x:changes(name)) then
    x:foreach(name, name, use_lang)
  end
  local result = luci.sys.exec(command)
  
  local plugins = {}
  local plugin
  
  for indent, line in result:gmatch ":?(%s*)([^\n]*)\n" do
    local level = math.floor(#indent/2)
    local field, value = line:match "([^:%s]+)%s*:%s*(.*)"
    if field and field ~= "" then
      if value then
        if value == "" and level == 0 then
          plugin = {}
          plugins[field] = plugin
        else
          plugin[field] = value
        end
      end
    end
  end
  
  return plugins
end

local plugins = list_plugins()
local p = s:option(StaticList, "plugins", translate("Enabled plugins"))

for name, info in pairs(plugins) do
  p:value(name, info.title)
end

s:option(Value, "master_toxid", translate("Master ToxID"), translate("The ToxID of bot owner (required for first start only)"))
s:option(Value, "shell_exec", translate("Shell Exec"), translate("The path to shell executable (for shell plugin)"))

return m
